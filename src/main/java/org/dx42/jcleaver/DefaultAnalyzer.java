package org.dx42.jcleaver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultAnalyzer implements Analyzer {

    public static final String DEFAULT_GROUP_SET_NAME = "default";
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultAnalyzer.class);
    
    /**
     * @param classInfo
     * @return
     */
    @Override
    public GroupSet analyze(ClassInfo classInfo) {
        Group group = new Group();
        group.addFields(classInfo.getFields());
        group.addMethods(classInfo.getMethods());
        
        List<Group> groups = new ArrayList<>();

        boolean done = false;
        while(!done) {
            Optional<Group> group2 = extractGroup(classInfo, group);
            done = !group2.isPresent();
            group2.ifPresent( g2-> addGroupIfNotEmpty(groups, g2));
        }

        Set<FieldInfo> ignoredFields = new HashSet<>();
        LOGGER.info("group=" + group);
        // TODO Also check if every field is static and final?
        if (group.getMethods().isEmpty()) {
            group.getFields().forEach(field -> {
                ignoredFields.add(field);
                group.removeField(field.getName());
            });
        }
        
        // Add the original group; may be empty at this point
        addGroupIfNotEmpty(groups, group);
        
        return new GroupSet(classInfo.getName(), DEFAULT_GROUP_SET_NAME, groups, ignoredFields);
    }
    
    private Optional<Group> extractGroup(ClassInfo classInfo, Group originalGroup) {
        Optional<MethodInfo> method = getArbitraryMethod(originalGroup);
        return method.map( m -> {
            Group newGroup = new Group();
            moveMethodAndItsReferences(classInfo, m, originalGroup, newGroup);
            return newGroup;
        });
    }
    
    private Optional<MethodInfo> getArbitraryMethod(Group group) {
        Set<MethodInfo> methods = group.getMethods();
        MethodInfo methodInfo = methods.isEmpty() ? null : new ArrayList<MethodInfo>(methods).get(0);
        return Optional.ofNullable(methodInfo);
    }

    private void addGroupIfNotEmpty(List<Group> groups,  Group group) {
        if (!group.isEmpty()) {
            LOGGER.info("Adding " + group);
            groups.add(group);
        }
    }
    
    private void moveMethodAndItsReferences(ClassInfo classInfo, MethodInfo method, Group fromGroup, Group toGroup) {
        if (toGroup.getMethod(method.getMethodId()) != null) {
            return;
        }
        
        moveMethod(method.getMethodId(), fromGroup, toGroup);
        
        moveFields(method.getReadFields(), fromGroup, toGroup);
        moveFields(method.getWriteFields(), fromGroup, toGroup);
        
        moveMethods(classInfo, method.getCallsMethods(), fromGroup, toGroup);
        moveMethods(classInfo, method.getCalledByMethods(), fromGroup, toGroup);
        
        // For remaining methods in this group, move any that reference fields in the other group
        fromGroup.getMethods().forEach(fromMethod -> {
            boolean readsToGroupFields = !Collections.disjoint(fromMethod.getReadFields(), toGroup.getFields()); 
            boolean writesToGroupFields = !Collections.disjoint(fromMethod.getWriteFields(), toGroup.getFields()); 
            if (readsToGroupFields || writesToGroupFields) {
                //LOGGER.info("Moving method " + fromMethod.getMethodId());
                moveMethodAndItsReferences(classInfo, fromMethod, fromGroup, toGroup);
            }
        });
    }

    private void moveFields(Set<FieldInfo> fields, Group fromGroup, Group toGroup) {
        fields.forEach(field -> {
            moveField(field.getName(), fromGroup, toGroup);
        });
    }

    private void moveMethods(ClassInfo classInfo, Set<MethodId> methods, Group fromGroup, Group toGroup) {
        methods.forEach(methodId -> {
            MethodInfo calledByMethod = classInfo.getMethod(methodId);
            if (calledByMethod != null) {
                moveMethodAndItsReferences(classInfo, calledByMethod, fromGroup, toGroup);
            }
        });
    }
    
    private void moveField(String fieldName, Group fromGroup, Group toGroup) {
        FieldInfo fieldInfo = fromGroup.getField(fieldName);
        if (fieldInfo != null) {
            toGroup.addField(fieldInfo);
            fromGroup.removeField(fieldName);
        }
    }

    private void moveMethod(MethodId methodId, Group fromGroup, Group toGroup) {
        MethodInfo methodInfo = fromGroup.getMethod(methodId);
        if (methodInfo != null) {
            toGroup.addMethod(methodInfo);
            fromGroup.removeMethod(methodId);
        }
    }
    
}
