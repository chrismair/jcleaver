package org.dx42.jcleaver;

public interface Analyzer {

    GroupSet analyze(ClassInfo classInfo);

}