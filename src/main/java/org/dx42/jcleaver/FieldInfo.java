package org.dx42.jcleaver;

/**
 * Represents the information for a single field
 *
 * @author Chris Mair
 */
public class FieldInfo {
    
    private final String name;
    private final boolean external;
    private final boolean staticFinal;

    public FieldInfo(String name) {
        this(name, false, false);
    }

    public FieldInfo(String name, boolean external, boolean staticFinal) {
        this.name = name;
        this.external = external;
        this.staticFinal = staticFinal;
    }
    
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        String externalSuffix = external ? "(external)" : "";
        String staticFinalSuffix = staticFinal ? "(static-final)" : "";
        return name + externalSuffix + staticFinalSuffix;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (external ? 1231 : 1237);
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FieldInfo other = (FieldInfo) obj;
        if (external != other.external)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    public boolean isExternal() {
        return external;
    }

    public boolean isStaticFinal() {
        return staticFinal;
    }
    
}
