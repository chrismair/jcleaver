/*
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.dx42.jcleaver;

import java.io.IOException;

import org.dx42.jcleaver.report.ClassInfoReportWriter;
import org.dx42.jcleaver.report.GroupSetReportWriter;
import org.dx42.jcleaver.report.TextClassInfoReportWriter;
import org.dx42.jcleaver.report.TextGroupSetReportWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JCleaver {

    protected ClassParser classParser = new AsmClassParser();
    protected Analyzer analyzer = new DefaultAnalyzer();
    protected GroupSetReportWriter reportWriter = new TextGroupSetReportWriter();
    protected ClassInfoReportWriter classInfoReportWriter = new TextClassInfoReportWriter();
    protected Logger logger = LoggerFactory.getLogger(JCleaver.class);
    
    public static void main(String[] args) {
        JCleaver jCleaver = new JCleaver();
        jCleaver.execute(args);
    }

    protected void execute(String[] args) {
        String baseDir = args[0];
        String className = args[1];
        logger.info("JCleaver command-line: baseDir=" + baseDir + "; className=" + className);
        try {
            ClassInfo classInfo = classParser.parseClassFile(baseDir, className);
            GroupSet groupSet = analyzer.analyze(classInfo);
            
            logger.info("JCleaver results:");
            logger.info(groupSet.toString());
            
            classInfoReportWriter.writeReport(classInfo);
            reportWriter.writeReport(groupSet);
        } catch (IOException e) {
            logger.info(e.toString());
        }
    }

}
