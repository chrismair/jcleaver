package org.dx42.jcleaver;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Group {

    private final Map<String, FieldInfo> fields = new HashMap<>();
    private final Map<MethodId, MethodInfo> methods = new HashMap<>();

    public Set<FieldInfo> getFields() {
        return asSet(fields.values());
    }
    
    public FieldInfo getField(String fieldName) {
        return fields.get(fieldName);
    }
    
    public void addField(FieldInfo fieldInfo) {
        fields.put(fieldInfo.getName(), fieldInfo);
    }
    
    public void addFields(Collection<FieldInfo> newFields) {
        newFields.forEach(f -> addField(f));
    }
    
    public void removeField(String fieldName) {
        fields.remove(fieldName);
    }
    
    public MethodInfo getMethod(MethodId methodId) {
        return methods.get(methodId);
    }
    
    public Set<MethodInfo> getMethods() {
        return asSet(methods.values());
    }
    
    public void addMethod(MethodInfo methodInfo) {
        methods.put(methodInfo.getMethodId(), methodInfo);
    }
    
    public void addMethods(Collection<MethodInfo> newMethods) {
        newMethods.forEach(m -> addMethod(m));
    }
    
    public void removeMethod(MethodId methodId) {
        methods.remove(methodId);
    }
    
    public boolean isEmpty() {
        return fields.isEmpty() && methods.isEmpty();
    }
    
    private <T> Set<T> asSet(Collection<T> collection) {
        return new HashSet<T>(collection);
    }

    @Override
    public String toString() {
        return "Group [fields=" + fields.keySet() + ", methods=" + methods + "]";
    }
    
}
