package org.dx42.jcleaver;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Represents the information for a single class
 *
 * @author Chris Mair
 */
public class ClassInfo {

    private final String name;
    private final Map<String, FieldInfo> fields = new HashMap<>();
    private final Map<MethodId, MethodInfo> methods = new HashMap<>();
    
    public ClassInfo(String name) {
        this.name = name;
    }
    
    public FieldInfo addField(String name) {
        FieldInfo fieldInfo = new FieldInfo(name);
        this.fields.put(name, fieldInfo);
        return fieldInfo;
    }
    
    public FieldInfo addExternalField(String name) {
        FieldInfo fieldInfo = new FieldInfo(name, true, false);
        this.fields.put(name, fieldInfo);
        return fieldInfo;
    }
    
    public FieldInfo addStaticFinalField(String name) {
        FieldInfo fieldInfo = new FieldInfo(name, false, true);
        this.fields.put(name, fieldInfo);
        return fieldInfo;
    }
    
    public MethodInfo addMethod(String name, String descriptor) {
        MethodId key = methodKey(name, descriptor);
        MethodInfo methodInfo = new MethodInfo(name, descriptor);
        this.methods.put(key, methodInfo);
        return methodInfo;
    }
    
    public String getName() {
        return name;
    }
    
    public FieldInfo getField(String fieldName) {
        return fields.get(fieldName);
    }
    
    public Set<FieldInfo> getFields() {
        return asSet(fields.values());
    }
    
    public MethodInfo getMethod(String name, String descriptor) {
        MethodId key = methodKey(name, descriptor);
        return methods.get(key);
    }
    
    public MethodInfo getMethod(MethodId methodId) {
        return methods.get(methodId);
    }
    
    public boolean containsMethod(MethodId methodId) {
        return methods.containsKey(methodId);
    }
    
    public Set<MethodInfo> getMethods() {
        return asSet(methods.values());
    }

    private MethodId methodKey(String name, String descriptor) {
        return new MethodId(name, descriptor);
    }
    
    private <T> Set<T> asSet(Collection<T> collection) {
        return new HashSet<T>(collection);
    }

    @Override
    public String toString() {
        return "ClassInfo [name=" + name + ", fields=" + fields + ", methods=" + methods + "]";
    }
    
}
