package org.dx42.jcleaver;

import java.util.HashSet;
import java.util.Set;

/**
 * Represents the information for a single method
 *
 * @author Chris Mair
 */
public class MethodInfo {
    
    private final MethodId methodId;
    private final Set<FieldInfo> readFields = new HashSet<>();
    private final Set<FieldInfo> writeFields = new HashSet<>();
    private final Set<FieldInfo> calledByFields = new HashSet<>();
    private final Set<MethodId> callsMethods = new HashSet<>();
    private final Set<MethodId> calledByMethods = new HashSet<>();

    public MethodInfo(String name, String descriptor) {
        this.methodId = new MethodId(name, descriptor);
    }

    public MethodId getMethodId() {
        return methodId;
    }
    
    public String getName() {
        return methodId.getName();
    }

    public String getDescriptor() {
        return methodId.getDescriptor();
    }
    
    public Set<FieldInfo> getReadFields() {
        return readFields;
    }

    public Set<FieldInfo> getWriteFields() {
        return writeFields;
    }
    
    public Set<FieldInfo> getCalledByFields() {
        return calledByFields;
    }
    
    public Set<MethodId> getCallsMethods() {
        return callsMethods;
    }

    public Set<MethodId> getCalledByMethods() {
        return calledByMethods;
    }

    public void addReadField(FieldInfo fieldInfo) {
        this.readFields.add(fieldInfo);
    }
    
    public void addWriteField(FieldInfo fieldInfo) {
        this.writeFields.add(fieldInfo);
    }
    
    public void addCalledByField(FieldInfo fieldInfo) {
        this.calledByFields.add(fieldInfo);
    }
    
    public void addCallsMethod(MethodId methodId) {
        this.callsMethods.add(methodId);
    }
    
    public void addCalledByMethod(MethodId methodId) {
        this.calledByMethods.add(methodId);
    }

    @Override
    public String toString() {
        return "MethodInfo [methodId=" + methodId + ", readFields=" + readFields + ", writeFields=" + writeFields
                + ", calledByFields=" + calledByFields + ", callsMethods=" + callsMethods + ", calledByMethods="
                + calledByMethods + "]";
    }

}
