package org.dx42.jcleaver;

import java.io.IOException;

public interface ClassParser {

    ClassInfo parseClass(String className) throws IOException;

    ClassInfo parseClassFile(String baseDir, String classFilePath) throws IOException;

}