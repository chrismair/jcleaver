package org.dx42.jcleaver;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Handle;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AsmClassParser implements ClassParser {

    private static final Logger LOGGER = LoggerFactory.getLogger(AsmClassParser.class);

    /**
     * Parse a named class within the current classloader.
     * 
     * @param className - the fully-qualified name of the class to load (e.g. "org.dx42.jcleaver.Sample")
     * @return the ClassInfo model for the specified class file
     * @throws IOException if an error occurs parsing the class
     */
    @Override
    public ClassInfo parseClass(String className) throws IOException {
        ClassReader classReader = new ClassReader(className);
        return parseClass(classReader);
    }
    
    /**
     * Parse the class file designated by a base directory and relative path.
     * 
     * @param baseDir - the base directory for class files
     * @param classFilePath - the path relative to baseDir of the class file to parse
     * @return the ClassInfo model for the specified class file
     * @throws IOException if an error occurs parsing the class file
     */
    @Override
    public ClassInfo parseClassFile(String baseDir, String classFilePath) throws IOException {
        String path = baseDir + "/" + classFilePath;
        FileInputStream inputStream = new FileInputStream(path);
        ClassReader classReader = new ClassReader(inputStream);
        return parseClass(classReader);
    }
    
    //------------------------------------------------------------------------------------
    // Helper methods
    //------------------------------------------------------------------------------------

    private ClassInfo parseClass(ClassReader classReader) {
        ParsingClassVisitor classVisitor = new ParsingClassVisitor();
        classReader.accept(classVisitor, ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES);
        addCalledByMethods(classVisitor.classInfo);
        return classVisitor.classInfo;
    }
    
    
    private void addCalledByMethods(ClassInfo classInfo) {
        for(MethodInfo method : classInfo.getMethods()) {
            for(MethodId callsMethodId : method.getCallsMethods()) {
                MethodId methodId = method.getMethodId();
                MethodInfo callsMethodInfo = classInfo.getMethod(callsMethodId);
                
                // callsMethodInfo can be null if it is a method defined in an ancestor class 
                if (callsMethodInfo == null) {
                    callsMethodInfo = classInfo.addMethod(callsMethodId.getName(), callsMethodId.getDescriptor());
                    LOGGER.info("Added " + callsMethodInfo);
                }
                callsMethodInfo.addCalledByMethod(methodId);
            }
        }
    }

    //------------------------------------------------------------------------------------
    //  ParsingMethodVisitor
    //------------------------------------------------------------------------------------

    private static class ParsingMethodVisitor extends MethodVisitor {
        
        private final ClassInfo classInfo;
        private final Context context;
        private final MethodInfo methodInfo;
        
        ParsingMethodVisitor(ClassInfo classInfo, MethodInfo methodInfo, Context context, int access, String name, String descriptor) {
            super(Opcodes.ASM6);
            this.classInfo = classInfo;
            this.context = context;
            this.methodInfo = methodInfo;
        }

        @Override
        public void visitFieldInsn(int opcode, String owner, String name, String descriptor) {
            if (isLocalField(owner, name)) {
                LOGGER.info("[" + methodInfo.getName() + "] visitFieldInsn: opcode=" + opcode + "; owner=" + owner + "; name=" + name + "; descriptor=" + descriptor);
                FieldInfo fieldInfo = getFieldInfo(name);
                if (opcode == Opcodes.GETFIELD || opcode == Opcodes.GETSTATIC) {
                    //LOGGER.info("Adding readOnlyFieldReference: " + name);
                    methodInfo.addReadField(fieldInfo);
                }
                if (opcode == Opcodes.PUTFIELD || opcode == Opcodes.PUTSTATIC) {
                    //LOGGER.info("Adding writeFieldReference: " + name);
                    methodInfo.addWriteField(fieldInfo);
                }
            }
            super.visitFieldInsn(opcode, owner, name, descriptor);
        }

        private boolean isLocalField(String owner, String name) {
            return owner.equals(classInfo.getName()) && !name.startsWith("__");
        }
        
        private FieldInfo getFieldInfo(String name) {
            FieldInfo fieldInfo = classInfo.getField(name);
            if (fieldInfo == null) {
                LOGGER.debug("getFieldInfo(): Adding field for " + name);
                fieldInfo = classInfo.addExternalField(name);
            }
            return fieldInfo;
        }
        
        @Override
        public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean itf) {
            if (isLocalMethodCall(owner, name)) {
                LOGGER.info("[" + methodInfo.getName() + "] visitMethodInsn: opcode=" + opcode + "; owner=" + owner + "; name=" + name + "; descriptor=" + descriptor);
                methodInfo.addCallsMethod(new MethodId(name, descriptor));
            }
            super.visitMethodInsn(opcode, owner, name, descriptor, itf);
        }
        
        private boolean isLocalMethodCall(String owner, String name) {
            //LOGGER.info("isLocalMethodCall: name=" + name + "; owner=" + owner);
            return owner.equals(classInfo.getName()) && !name.startsWith("$");
        }

        @Override
        public void visitInvokeDynamicInsn(String name, String descriptor, Handle bsm, Object... bsmArgs) {
            LOGGER.info("[" + methodInfo.getName() + "] visitInvokeDynamicInsn: name=" + name + "; descriptor=" + descriptor + "; handle=" + bsm + "; bsmArgs=" + Arrays.asList(bsmArgs));
            Arrays.stream(bsmArgs).forEach(a -> { 
                LOGGER.debug("    arg: " + a + "; class=" + a.getClass());
                if (a instanceof Handle) {
                    Handle handle = (Handle)a;
                    LOGGER.debug("      handle: owner=" + handle.getOwner() + "; name=" + handle.getName() + "; desc=" + handle.getDesc());
                    if (isLocalLambda(handle.getName(), handle.getOwner())) {
                        MethodId lambdaMethodId = new MethodId(handle.getName(), handle.getDesc());
                        MethodId calledFromMethodId = methodInfo.getMethodId();
                        context.lambdaMethodCalls.put(lambdaMethodId, calledFromMethodId);
                        LOGGER.debug("  -- Adding local lambda; from " + lambdaMethodId + " ... called from ... " + calledFromMethodId);
                    }
                }
            });
            super.visitInvokeDynamicInsn(name, descriptor, bsm, bsmArgs);
        }
        
        private boolean isLocalLambda(String name, String owner) {
            return name.startsWith("lambda$") && owner.equals(classInfo.getName());
        }

    }

    //------------------------------------------------------------------------------------
    // ParsingClassVisitor
    //------------------------------------------------------------------------------------

    private static class ParsingClassVisitor extends ClassVisitor {

        ClassInfo classInfo;
        Context context;
        
        ParsingClassVisitor() {
            super(Opcodes.ASM6);
            context = new Context();
        }

        @Override
        public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
            this.classInfo = new ClassInfo(name);
            super.visit(version, access, name, signature, superName, interfaces);
        }
        
        @Override
        public FieldVisitor visitField(int access, String name, String desc, String signature, Object value) {
            LOGGER.info("visitField:  access=" + access + "; name=" + name + "; descriptor=" + desc + "; signature=" + signature + "; value=" + value);
            boolean isSynthetic = (access & Opcodes.ACC_SYNTHETIC) != 0;
            boolean ignoreField = isSynthetic; 
            if (!ignoreField) {
                boolean staticFinal = isStatic(access) && isFinal(access);
                LOGGER.info("field " + name + ": access=" + access + "; staticFinal=" + staticFinal);
                if (staticFinal) {
                    classInfo.addStaticFinalField(name);
                }
                else {
                    classInfo.addField(name);
                }
            }
            return super.visitField(access, name, desc, signature, value);
        }

        @Override
        public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
            LOGGER.info("visitMethod:  access=" + access + "; name=" + name + "; descriptor=" + descriptor + "; signature=" + signature + "; exceptions=" + exceptions);
            if (isValidMethod(access, name)) {
                MethodId methodId = new MethodId(name, descriptor);
                MethodInfo methodInfo;
                if (isSynthetic(access) && context.lambdaMethodCalls.containsKey(methodId)) {
                    MethodId calledByMethodInfo = context.lambdaMethodCalls.get(methodId);
                    methodInfo = classInfo.getMethod(calledByMethodInfo);
                    LOGGER.debug("   ** Using " + calledByMethodInfo);
                }
                else {
                    methodInfo = classInfo.addMethod(name, descriptor);
                }
                return new ParsingMethodVisitor(classInfo, methodInfo, context, access, name, descriptor);
            }
            else {
                return super.visitMethod(access, name, descriptor, signature, exceptions);
            }
        }
        
        private boolean isSynthetic(int access) {
            return (access & Opcodes.ACC_SYNTHETIC) != 0;
        }

        private boolean isStatic(int access) {
            return (access & Opcodes.ACC_STATIC) != 0;
        }
        
        private boolean isFinal(int access) {
            return (access & Opcodes.ACC_FINAL) != 0;
        }
        
        private boolean isValidMethod(int access, String name) {
            return !name.startsWith("<");
        }

    }

    private static class Context {
        // Map of lambda method -> method that called it
        private Map<MethodId, MethodId> lambdaMethodCalls = new HashMap<>();
    }
    
}
