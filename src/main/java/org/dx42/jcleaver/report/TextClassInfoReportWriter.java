package org.dx42.jcleaver.report;

import java.io.PrintWriter;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.dx42.jcleaver.ClassInfo;
import org.dx42.jcleaver.FieldInfo;
import org.dx42.jcleaver.MethodId;
import org.dx42.jcleaver.MethodInfo;

public class TextClassInfoReportWriter implements ClassInfoReportWriter {

    private static final String INDENT = "    ";
    private static final String INDENT_2 = INDENT + INDENT;
    private static final String INDENT_3 = INDENT_2 + INDENT;
    private static final String INDENT_4 = INDENT_3 + INDENT;
    
    /**
     * Write out the report of class information
     * @param classInfo - the ClassInfo to report
     */
    @Override
    public void writeReport(ClassInfo classInfo) {
        PrintWriter writer = new PrintWriter(System.out);
        
        writer.println("Class: " + classInfo.getName());
        writer.println();
        writer.println(INDENT + "Fields:");
        for(String fieldName : getFieldNames(classInfo.getFields())) {
            writer.println(INDENT_2 + "- " + fieldName);
        }
        
        writer.println();
        writer.println(INDENT + "Methods:");
        for(MethodInfo methodInfo : getSortedMethods(classInfo.getMethods())) {
            writer.println(INDENT_2 + "- " + methodInfo.getName());

            writer.println(INDENT_3 + "readFields:");
            for(String fieldName : getFieldNames(methodInfo.getReadFields())) {
                writer.println(INDENT_4 + "- " + fieldName);
            }

            writer.println(INDENT_3 + "writeFields:");
            for(String fieldName : getFieldNames(methodInfo.getWriteFields())) {
                writer.println(INDENT_4 + "- " + fieldName);
            }
            
            writer.println(INDENT_3 + "callsMethods:");
            for(MethodId callsMethodId : getSortedMethodIds(methodInfo.getCallsMethods())) {
                writer.println(INDENT_4 + "- " + callsMethodId.getName());
            }

            writer.println(INDENT_3 + "calledByMethods:");
            for(MethodId calledByMethodId : getSortedMethodIds(methodInfo.getCalledByMethods())) {
                writer.println(INDENT_4 + "- " + calledByMethodId.getName());
            }
        }
        writer.flush();
    }
    
    private SortedSet<String> getFieldNames(Set<FieldInfo> fields) {
        return fields.stream()
                .map(fieldInfo -> fieldInfo.getName())
                .collect(Collectors.toCollection(TreeSet::new));
    }
    
    private List<MethodInfo> getSortedMethods(Set<MethodInfo> methods) {
        return methods.stream()
                .sorted((o1, o2) -> o1.getName().compareTo(o2.getName()))
                .collect(Collectors.toList());
    }
    
    private List<MethodId> getSortedMethodIds(Set<MethodId> methodIds) {
        return methodIds.stream()
                .sorted((o1, o2) -> o1.getName().compareTo(o2.getName()))
                .collect(Collectors.toList());
    }
    
}
