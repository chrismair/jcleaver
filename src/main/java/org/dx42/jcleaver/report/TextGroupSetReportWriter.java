package org.dx42.jcleaver.report;

import java.io.PrintWriter;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.dx42.jcleaver.FieldInfo;
import org.dx42.jcleaver.Group;
import org.dx42.jcleaver.GroupSet;

public class TextGroupSetReportWriter implements GroupSetReportWriter {

    private static final String INDENT = "    ";
    private static final String INDENT_2 = INDENT + INDENT;
    private static final String INDENT_3 = INDENT_2 + INDENT;
    
    /**
     * Write a GroupSet report
     * @param groupSet - the GroupSet to write
     */
    @Override
    public void writeReport(GroupSet groupSet) {
        PrintWriter writer = new PrintWriter(System.out);
        
        writer.println("Class: " + groupSet.getClassName());
        writer.println("GroupSet: " + groupSet.getName());
        int index = 0;
        for(Group group : groupSet.getGroups()) {
            writer.println();
            writer.println(INDENT + "Group " + index++);
            
            writer.println(INDENT_2 + "Fields:");
            for(String fieldName : getFieldNames(group.getFields())) {
                writer.println(INDENT_3 + "- " + fieldName);
            }
            
            writer.println(INDENT_2 + "Methods:");
            for(String methodName : getMethodNames(group)) {
                writer.println(INDENT_3 + "- " + methodName);
            }
        }
        
        if (!groupSet.getIgnoredFields().isEmpty()) {
            writer.println();
            writer.println(INDENT + "Ignored Fields:");
            for(String fieldName : getFieldNames(groupSet.getIgnoredFields())) {
                writer.println(INDENT_2 + "- " + fieldName);
            }
        }

        writer.flush();
    }
    
    private SortedSet<String> getFieldNames(Set<FieldInfo> fields) {
        return fields.stream()
                .map(fieldInfo -> fieldInfo.getName())
                .collect(Collectors.toCollection(TreeSet::new));
    }
    
    private SortedSet<String> getMethodNames(Group group) {
        return group.getMethods().stream()
                .map(methodInfo -> methodInfo.getName())
                .collect(Collectors.toCollection(TreeSet::new));
    }
    
}
