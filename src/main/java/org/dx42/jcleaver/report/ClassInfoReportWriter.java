package org.dx42.jcleaver.report;

import org.dx42.jcleaver.ClassInfo;

public interface ClassInfoReportWriter {

    /**
     * Write a ClassInfo report
     * @param classInfo - the ClassInfo to write
     */
    void writeReport(ClassInfo classInfo);

}