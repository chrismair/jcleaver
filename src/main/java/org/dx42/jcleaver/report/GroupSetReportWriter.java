package org.dx42.jcleaver.report;

import org.dx42.jcleaver.GroupSet;

public interface GroupSetReportWriter {

    void writeReport(GroupSet groupSet);

}