package org.dx42.jcleaver;

import java.util.List;
import java.util.Set;

public class GroupSet {

    private final String className;
    private final String name;
    private final List<Group> groups;
    private final Set<FieldInfo> ignoredFields;
    
    public GroupSet(String className, String name, List<Group> groups, Set<FieldInfo> ignoredFields) {
        this.className = className;
        this.name = name;
        this.groups = groups;
        this.ignoredFields = ignoredFields;
    }

    public String getName() {
        return name;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public String getClassName() {
        return className;
    }
    
    @Override
    public String toString() {
        return "GroupSet [className=" + className + ", name=" + name + ", groups=" + groups + ", ignoredFields="
                + ignoredFields + "]";
    }

    public Set<FieldInfo> getIgnoredFields() {
        return ignoredFields;
    }

}
