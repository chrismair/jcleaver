package org.dx42.jcleaver.sample;

public class Sample_StaticFields {

    private static final String CONSTANT1 = "ok";
    private static final int CONSTANT2;

    protected static int counter;
    
    static {
        CONSTANT2 = 111;
    }
    
    // Group 1
    
    public void method1() {
        System.out.println("value=" + method2(99));
    }
    
    public String method2(int add) {
        return CONSTANT1 + CONSTANT2 + add;     // CONSTANT1 value will be inlined
    }

    // Group 2
    
    public String method3() {
        method4();
        counter++;
        return "answer=" + counter;
    }

    public void method4() {
        counter = 44;
    }

}
