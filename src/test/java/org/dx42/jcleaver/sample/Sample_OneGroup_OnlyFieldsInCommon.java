package org.dx42.jcleaver.sample;

public class Sample_OneGroup_OnlyFieldsInCommon {

    private String field1;
    private String field2 = "abc";
    private int field3 = 33;
    private int field4;
    
    public void method1() {
        System.out.println("value=" + field1);
    }
    
    public int method2(int add) {
        return field3 + field4 + add;
    }
    
    public String method3() {
        return field1 + ":" + field2 + ":" + field3;
    }

    public void method4() {
        field4 = 44;
    }

}
