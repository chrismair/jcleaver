package org.dx42.jcleaver.sample;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Sample_Lambdas {

    private String field1;
    private List<String> field2 = Arrays.asList("abc");
    private String field3 = "33";
    private int field4;
    private int field5;
    private int field6;
    
    // Group 1
    
    public void method1() {
        System.out.println("value=" + method2(99));
    }
    
    public String method2(int add) {
        List<String> sortedList = field2.stream()
                .sorted((o1, o2) -> o1.compareTo(o2))
                .collect(Collectors.toList());
        return field1 + sortedList + add;
    }

    // Group 2
    
    public String method3() throws Exception {
        method4();
        field3 = field3 + "xxx";
        Function<String, String> function = (prefix) -> {
            return prefix + field3.hashCode() + field6;
        };
        Callable<String> callable = () -> function.apply("hashCode: " + field5);
        return "Value: " + callable.call();
    }

    public void method4() {
        field4 = 44;
    }

}
