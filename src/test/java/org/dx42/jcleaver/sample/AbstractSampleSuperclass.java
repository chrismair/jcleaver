package org.dx42.jcleaver.sample;

public class AbstractSampleSuperclass {

    protected String superField1 = "sample";
    protected int superField2 = 99;
    
    public void superMethod1() {
        System.out.println("ok");
    }
    
    public int superMethod2(int add) {
        return 88;
    }

}
