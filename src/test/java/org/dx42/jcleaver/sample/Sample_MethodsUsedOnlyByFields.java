package org.dx42.jcleaver.sample;

public class Sample_MethodsUsedOnlyByFields {

    private static final String FIELD1 = calculateField1();
    public static String field2 = calculateField2();
    
    public String field3 = calculateField3();
    private String field4 = calculateField4();
    private String field5 = "field5";
    
    private static String calculateField1() {
        return "abc";
    }
    
    private static String calculateField2() {
        return "field2";
    }
    
    private static String calculateField3() {   // static method
        return "field3";
    }
    
    private String calculateField4() {          // instance method
        return "field4";
    }
    
    
    public void method1() {
        System.out.println("values=" + FIELD1 + ", " + field4 );
    }
    
    public String method2() {
        return field5;
    }

}
