package org.dx42.jcleaver.sample;

public class Sample_UsesSuperclassFields extends AbstractSampleSuperclass {

    private int field1 = 44;
    private String field2 = "abc";
    
    public void method1() {
        System.out.println("value=" + superField1);
    }
    
    public int method2(int add) {
        return field1 + superField2 + add;
    }
    
    public String method3() {
        return "result:" + field1 + ":" + field2 + ":" + superField1;
    }

}
