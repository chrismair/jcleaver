package org.dx42.jcleaver.sample;

public class Sample_ThreeGroups {

    private String field1;
    private String field2 = "abc";
    private int field3 = 33;
    private int field4;
    private int field5;
    
    // Group 1
    
    public void method1() {
        System.out.println("value=" + method2(99));
    }
    
    public String method2(int add) {
        return field1 + field2 + add;
    }

    // Group 2
    
    public String method3() {
        method4();
        field3 += field4;
        return "answer" + field3;
    }

    public void method4() {
        field4 = 44;
    }

    // Group 3
    
    public int method5() {
        field5 += 22;
        return field5;
    }
    
}
