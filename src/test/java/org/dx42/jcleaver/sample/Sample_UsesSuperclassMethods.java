package org.dx42.jcleaver.sample;

public class Sample_UsesSuperclassMethods extends AbstractSampleSuperclass {

    private int field1 = 44;
    //private String field2 = "abc";
    
    public void method1() {
        superMethod1();
    }
    
    public int method2(int add) {
        return field1 + superMethod2(add) + add;
    }
    
    public String method3() {
        superMethod1();
        return "ok";
    }

}
