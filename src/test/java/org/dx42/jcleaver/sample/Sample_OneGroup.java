package org.dx42.jcleaver.sample;

public class Sample_OneGroup {

    private String field1;
    private String field2 = "abc";
    private int field3 = 33;
    private int field4;
    
    public void method1() {
        System.out.println("value=" + method2(99));
    }
    
    public int method2(int add) {
        return field3 + add;
    }
    
    public String method3() {
        method4();
        field3 += method2(11);
        return field1 + ":" + field2 + ":" + field3;
    }

    public void method4() {
        field4 = 44;
    }

}
