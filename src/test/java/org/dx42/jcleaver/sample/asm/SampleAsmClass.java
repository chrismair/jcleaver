package org.dx42.jcleaver.sample.asm;

import static java.lang.Math.abs;

import java.util.function.Function;

public class SampleAsmClass extends AbstractSampleAsmClass {

    private static final String CONSTANT1 = "ok";
    private static final int CONSTANT2;
    protected static int counter;
    
    static {
        CONSTANT2 = 111;
    }
            
    private String field1;
    private String field2 = "abc";
    private int field3 = 33;
    private int field4;
    
    public void method1() {
        System.out.println("value=" + method2(99));
        doWithName(name);      // method and field on ancestor class
    }
    
    public int method2(int add) {
        return field3 + add + CONSTANT2 + counter++;
    }
    
    public String method3() {
        method4();
        field3 += method2(11);
        Function<String, String> function = (s) -> {
            String prefix = s;
            return prefix + field1.hashCode();
        };
        String prefix = "Value: " + CONSTANT1;      // CONSTANT1 value is inlined
        return prefix + function.apply("hashCode: ");
    }

    public void method4() {
        field4 = 44;
        doStuff();      // method on superclass
        System.out.println("abs=" + abs(-23));   // Math.abs()
    }

}
