package org.dx42.jcleaver

import org.junit.Test

/**
 * Tests for Group
 *
 * @author Chris Mair
 */
class GroupTest extends AbstractTestCase {

    private static final String FIELD_NAME = 'field1'
    private static final String FIELD_NAME2 = 'field2'
    private static final FieldInfo FIELD_INFO = new FieldInfo('field1')
    private static final FieldInfo FIELD_INFO2 = new FieldInfo('field2')
    
    private static final String METHOD_NAME = 'method1'
    private static final String METHOD_NAME2 = 'method2'
    private static final String METHOD_DESCRIPTOR = 'descriptor1'
    private static final String METHOD_DESCRIPTOR2 = 'descriptor2'
    private static final MethodId METHOD_ID = new MethodId(METHOD_NAME, METHOD_DESCRIPTOR)
    private static final MethodInfo METHOD_INFO = new MethodInfo(METHOD_NAME, METHOD_DESCRIPTOR)
    private static final MethodInfo METHOD_INFO2 = new MethodInfo(METHOD_NAME2, METHOD_DESCRIPTOR2)
    
    private Group group = new Group()
    
    @Test
    void test_InitialValues() {
        assert group.getFields() == [] as Set
        assert group.getMethods() == [] as Set
    }

    @Test
    void test_Fields() {
        assert group.isEmpty()
        assert group.getField('NoSuchField') == null
        
        group.addField(FIELD_INFO)
        assert group.getFields() == [FIELD_INFO] as Set
        assert group.getField(FIELD_NAME) == FIELD_INFO
        assert group.isEmpty() == false
        
        group.addFields([FIELD_INFO2])
        assert group.getFields() == [FIELD_INFO, FIELD_INFO2] as Set
        
        group.removeField(FIELD_NAME)
        assert group.getFields() == [FIELD_INFO2] as Set
    }
    
    @Test
    void test_Methods() {
        assert group.isEmpty()
        assert group.getMethod(new MethodId('name', 'descriptor')) == null
        
        group.addMethod(METHOD_INFO)
        assert group.getMethods() == [METHOD_INFO] as Set
        assert group.getMethod(METHOD_ID) == METHOD_INFO
        
        group.addMethods([METHOD_INFO2])
        assert group.getMethods() == [METHOD_INFO, METHOD_INFO2] as Set
        assert group.isEmpty() == false
        
        group.removeMethod(METHOD_ID)
        assert group.getMethods() == [METHOD_INFO2] as Set
    }

}
