package org.dx42.jcleaver

import org.dx42.jcleaver.sample.asm.SampleAsmClass
import org.junit.Test

/**
 * Tests for AsmClassParser
 *
 * @author Chris Mair
 */
class AsmClassParserTest extends AbstractTestCase {

    private Set<String> EMPTY_SET = [] as Set
    private String CLASS_FILES_BASE_DIR = 'bin'     // Eclipse-specific
    private String CLASS_NAME = 'org/dx42/jcleaver/sample/asm/SampleAsmClass'
    
    private AsmClassParser classParser = new AsmClassParser()
    private ClassInfo classInfo
    
    @Test
    void test_ImplementsClassParser() {
        assert classParser instanceof ClassParser
    }
    
    // Tests for parseClass(String)
    
    @Test
    void test_parseClass_String() {
        classInfo = classParser.parseClass(SampleAsmClass.getName())
        assertClassInfoForSampleClass()
    }
    
    @Test
    void test_parseClass_String_ClassNameDoesNotExist() {
        shouldFail(IOException) { classParser.parseClass('NoSuchClass') }
    }
    
    // Tests for parseClassFile(String)

    @Test
    void test_parseClassFile_String() {
        classInfo = classParser.parseClassFile(CLASS_FILES_BASE_DIR, '/org/dx42/jcleaver/sample/asm/SampleAsmClass.class')
        assertClassInfoForSampleClass()
    }
    
    @Test
    void test_parseClassFile_String_ClassNameDoesNotExist() {
        shouldFail(IOException) { classParser.parseClassFile(CLASS_FILES_BASE_DIR, 'NoSuchClass') }
    }

    //------------------------------------------------------------------------------------
    // Helper methods
    //------------------------------------------------------------------------------------

    private void assertClassInfoForSampleClass() {
        log(classInfo)
        assert classInfo.getName() == CLASS_NAME
        assert classInfo.getFields().name as Set == ['field1', 'field2', 'field3', 'field4', 'name', 'CONSTANT1', 'CONSTANT2', 'counter'] as Set
        assert classInfo.getMethods().name as Set == ['method1', 'method2', 'method3', 'method4', 'doWithName', 'doStuff'] as Set

        def method1 = classInfo.getMethod('method1', '()V')
        def method2 = classInfo.getMethod('method2', '(I)I')
        def method3 = classInfo.getMethod('method3', '()Ljava/lang/String;')
        def method4 = classInfo.getMethod('method4', '()V')
        def doWithName = classInfo.getMethod('doWithName', '(Ljava/lang/String;)V')
        def doStuff = classInfo.getMethod('doStuff', '()V')
        
        def field1 = classInfo.getField('field1')
        def field2 = classInfo.getField('field2')
        def field3 = classInfo.getField('field3')
        def field4 = classInfo.getField('field4')
        def name = classInfo.getField('name')
        def constant1 = classInfo.getField('CONSTANT1')
        def constant2 = classInfo.getField('CONSTANT2')
        def counter = classInfo.getField('counter')
        
        assert constant1.isStaticFinal() == true
        assert constant2.isStaticFinal() == true
        assert counter.isStaticFinal() == false
        assert name.isStaticFinal() == false
        
        assertFieldReferences(method1, [name], EMPTY_SET)
        assertFieldReferences(method2, [field3, counter, constant2], [counter])
        assertFieldReferences(method3, [field1, field3], [field3])      // constant1 value is inlined
        assertFieldReferences(method4, EMPTY_SET, [field4])
        
        assertMethodReferences(method1, [method2, doWithName], EMPTY_SET)
        assertMethodReferences(method2, [], [method1, method3])
        assertMethodReferences(method3, [method2, method4], EMPTY_SET)
        assertMethodReferences(method4, [doStuff], [method3])
    }
    
    private void assertFieldReferences(MethodInfo methodInfo, Collection<FieldInfo> readFieldNames, Collection<FieldInfo> writeFieldNames) {
        assert methodInfo.readFields as Set == readFieldNames as Set
        assert methodInfo.writeFields as Set == writeFieldNames as Set
    }
    
    private void assertMethodReferences(MethodInfo methodInfo, Collection<MethodInfo> callsMethods, Collection<MethodInfo> calledByMethods) {
        assert methodInfo.callsMethods as Set == callsMethods.methodId as Set
        assert methodInfo.calledByMethods as Set == calledByMethods.methodId as Set
    }
    
    private Set<FieldInfo> fieldInfoSet(Collection<String> fieldNames) {
        return fieldNames.collect { name -> new FieldInfo(name) } as Set
    }
}
