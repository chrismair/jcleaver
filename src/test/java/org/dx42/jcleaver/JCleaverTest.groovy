/*
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.dx42.jcleaver

import org.dx42.jcleaver.report.ClassInfoReportWriter
import org.dx42.jcleaver.report.GroupSetReportWriter
import org.dx42.jcleaver.report.TextClassInfoReportWriter
import org.dx42.jcleaver.report.TextGroupSetReportWriter
import org.junit.Before
import org.junit.Test
import org.slf4j.Logger

class JCleaverTest extends AbstractTestCase {

    private static final String BASE_DIR = "base/dir"
    private static final String CLASS_NAME = "org/dx42/SomeClass.class"
    private static final ClassInfo CLASS_INFO = new ClassInfo("someClass")
    private static final GroupSet GROUP_SET = new GroupSet("class", "groupSet", [], [] as Set)
    private static final IOException IO_EXCEPTION = new IOException("bad") 

    private JCleaver jCleaver = new JCleaver()
    private Map called = [:]
    private List loggedMessages = []

    @Test
    void test_InitialValues() {
        jCleaver = new JCleaver()
        assert jCleaver.classParser instanceof AsmClassParser
        assert jCleaver.analyzer instanceof Analyzer
        assert jCleaver.reportWriter instanceof TextGroupSetReportWriter
        assert jCleaver.classInfoReportWriter instanceof TextClassInfoReportWriter
    }
    
    @Test
    void test_execute() {
        jCleaver.execute([BASE_DIR, CLASS_NAME] as String[])

        assert called.parseClassFile
        assert called.analyze
        assert called.writeReport
        assert called.classInfoWriteReport
        
        log("loggedMessages=" + loggedMessages)
        assert loggedMessages.contains(GROUP_SET.toString())
    }

    @Test
    void test_execute_ClassParser_ThrowsIOException() {
        jCleaver.classParser = [
            parseClassFile:{ baseDir, classFile ->
                throw IO_EXCEPTION
            }] as ClassParser
        
        jCleaver.execute([BASE_DIR, CLASS_NAME] as String[])
        assert loggedMessages.contains(IO_EXCEPTION.toString())
    }

    // TODO Test for main()

    //------------------------------------------------------------------------------------
    // Setup and helper methods
    //------------------------------------------------------------------------------------
    
    @Before
    void before() {
        jCleaver.classParser = [
            parseClassFile:{ baseDir, classFile ->
                assert baseDir == BASE_DIR
                assert classFile == CLASS_NAME
                called.parseClassFile = true
                return CLASS_INFO
            }] as ClassParser

        jCleaver.analyzer = [
            analyze:{ classInfo ->
                assert classInfo == CLASS_INFO
                called.analyze = true
                return GROUP_SET
            }] as Analyzer

        jCleaver.reportWriter = [
            writeReport:{ groupSet -> 
                assert groupSet == GROUP_SET
                called.writeReport = true
            }] as GroupSetReportWriter
        
        jCleaver.classInfoReportWriter = [
            writeReport:{ classInfo -> 
                assert classInfo == CLASS_INFO
                called.classInfoWriteReport = true
            }] as ClassInfoReportWriter
        
        jCleaver.logger = [info:{ message -> loggedMessages << message }] as Logger
    }

}
