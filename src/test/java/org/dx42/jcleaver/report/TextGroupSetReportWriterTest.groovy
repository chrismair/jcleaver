package org.dx42.jcleaver.report

import org.dx42.jcleaver.AbstractTestCase
import org.dx42.jcleaver.FieldInfo
import org.dx42.jcleaver.Group
import org.dx42.jcleaver.GroupSet
import org.dx42.jcleaver.MethodInfo
import org.junit.Before
import org.junit.Test

/**
 * Tests for TextGroupSetReportWriter
 *
 * @author Chris Mair
 */
class TextGroupSetReportWriterTest extends AbstractTestCase {

    private static final FieldInfo FIELD_INFO = new FieldInfo('field1')
    private static final FieldInfo FIELD_INFO2 = new FieldInfo('field2')
    private static final FieldInfo FIELD_INFO3 = new FieldInfo('field3')
    private static final FieldInfo FIELD_INFO4 = new FieldInfo('field4')
    private static final FieldInfo FIELD_INFO5 = new FieldInfo('field5')
    private static final MethodInfo METHOD_INFO = new MethodInfo('method1', 'descriptor1')
    private static final MethodInfo METHOD_INFO2 = new MethodInfo('method2', 'descriptor2')
    private static final MethodInfo METHOD_INFO3 = new MethodInfo('method3', 'descriptor3')

    private Group group1 = new Group()
    private Group group2 = new Group()
    private GroupSet groupSet = new GroupSet("MyClass", "MyGroupSet", [group1, group2], [] as Set)
    private GroupSet groupSetIgnoredFields = new GroupSet("MyClass", "MyGroupSet", [group1, group2], [FIELD_INFO5, FIELD_INFO4] as Set)

    private GroupSetReportWriter reportWriter = new TextGroupSetReportWriter()

    @Test
    void test_writeReport() {
        String output = captureSystemOut { reportWriter.writeReport(groupSet) }
        log(output)
        assert output.trim() == '''
Class: MyClass
GroupSet: MyGroupSet

    Group 0
        Fields:
            - field1
            - field2
        Methods:
            - method1
            - method2

    Group 1
        Fields:
            - field3
        Methods:
            - method3
        '''.trim()
    }

    @Test
    void test_writeReport_IgnoreFields() {
        String output = captureSystemOut { reportWriter.writeReport(groupSetIgnoredFields) }
        log(output)
        assert output.trim() == '''
Class: MyClass
GroupSet: MyGroupSet

    Group 0
        Fields:
            - field1
            - field2
        Methods:
            - method1
            - method2

    Group 1
        Fields:
            - field3
        Methods:
            - method3

    Ignored Fields:
        - field4
        - field5
        '''.trim()
    }

    @Before
    void setUp() {
        group1.addField(FIELD_INFO)
        group1.addField(FIELD_INFO2)
        group1.addMethod(METHOD_INFO)
        group1.addMethod(METHOD_INFO2)
        
        group2.addField(FIELD_INFO3)
        group2.addMethod(METHOD_INFO3)
    }
}
