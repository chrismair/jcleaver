package org.dx42.jcleaver.report

import static org.junit.Assert.*

import org.dx42.jcleaver.AbstractTestCase
import org.dx42.jcleaver.ClassInfo
import org.dx42.jcleaver.FieldInfo
import org.dx42.jcleaver.Group
import org.dx42.jcleaver.GroupSet
import org.dx42.jcleaver.MethodInfo
import org.junit.Before
import org.junit.Test

/**
 * Tests for TextGroupSetReportWriter
 *
 * @author Chris Mair
 */
class TextClassInfoReportWriterTest extends AbstractTestCase {

    private ClassInfoReportWriter reportWriter = new TextClassInfoReportWriter()
    private ClassInfo classInfo = new ClassInfo('MyClass')

    @Test
    void test_writeReport() {
        String output = captureSystemOut { reportWriter.writeReport(classInfo) }
        log(output)
        String expected = '''
Class: MyClass

    Fields:
        - field1
        - field2
        - field3

    Methods:
        - method1
            readFields:
                - field1
            writeFields:
                - field1
            callsMethods:
            calledByMethods:
                - method3
        - method2
            readFields:
            writeFields:
            callsMethods:
            calledByMethods:
        - method3
            readFields:
                - field2
            writeFields:
                - field3
            callsMethods:
                - method1
            calledByMethods:

        '''.trim()
        assertEquals(expected, output.trim())
    }

    @Before
    void setUp() {
        classInfo.addField('field1')
        classInfo.addField('field2')
        classInfo.addField('field3')
        classInfo.addMethod('method1', 'descriptor1')
        classInfo.addMethod('method2', 'descriptor2')
        classInfo.addMethod('method3', 'descriptor3')
        
        def field1 = classInfo.getField('field1')
        def field2 = classInfo.getField('field2')
        def field3 = classInfo.getField('field3')
        def method1 = classInfo.getMethod('method1', 'descriptor1')
        def method2 = classInfo.getMethod('method2', 'descriptor2')
        def method3 = classInfo.getMethod('method3', 'descriptor3')
        
        method1.addReadField(field1)
        method1.addWriteField(field1)
        method1.addCalledByMethod(method3.getMethodId())
        
        method3.addReadField(field2)
        method3.addWriteField(field3)
        method3.addCallsMethod(method1.getMethodId())
    }
}
