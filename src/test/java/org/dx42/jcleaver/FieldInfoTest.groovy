package org.dx42.jcleaver

import org.junit.Test

/**
 * Tests for FieldInfo
 *
 * @author Chris Mair
 */
class FieldInfoTest extends AbstractTestCase {

    private static final String FIELD_NAME = 'field123'
    
    private FieldInfo fieldInfo =  new FieldInfo(FIELD_NAME)
    private FieldInfo fieldInfo2 =  new FieldInfo("other")
    
    @Test
    void test_Constructor() {
        assert fieldInfo.getName() == FIELD_NAME
    }

    @Test
    void test_Equals() {
        assert fieldInfo == fieldInfo
        assert fieldInfo == new FieldInfo(FIELD_NAME)
        
        assert fieldInfo != new FieldInfo(FIELD_NAME, true, true)
        assert fieldInfo != fieldInfo2
        assert fieldInfo != null
    }
    
    @Test
    void test_hashCode() {
        assert fieldInfo.hashCode() == fieldInfo.hashCode()
        assert fieldInfo.hashCode() == new FieldInfo(FIELD_NAME).hashCode()
        
        assert fieldInfo.hashCode() != new FieldInfo(FIELD_NAME, true, true).hashCode()
        assert fieldInfo.hashCode() != fieldInfo2.hashCode()
    }
    
    @Test
    void test_toString() {
        assert fieldInfo.toString() == FIELD_NAME
        assert new FieldInfo(FIELD_NAME, true, false).toString() == FIELD_NAME + '(external)'
        assert new FieldInfo(FIELD_NAME, true, true).toString() == FIELD_NAME + '(external)(static-final)'
        assert new FieldInfo(FIELD_NAME, false, true).toString() == FIELD_NAME + '(static-final)'
    }
    
}
