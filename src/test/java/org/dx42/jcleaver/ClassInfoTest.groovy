package org.dx42.jcleaver

import org.junit.Test

/**
 * Tests for ClassInfo
 *
 * @author Chris Mair
 */
class ClassInfoTest extends AbstractTestCase {

    private static final String CLASS_NAME = 'class123'
    
    private static final String FIELD_NAME = 'field1'
    private static final String FIELD_NAME2 = 'field2'
    private static final String FIELD_NAME3 = 'field3'
    
    private static final String METHOD_NAME = 'method1'
    private static final String METHOD_NAME2 = 'method2'
    private static final String METHOD_DESCRIPTOR = 'descriptor1'
    private static final String METHOD_DESCRIPTOR2 = 'descriptor2'
    private static final MethodId METHOD_ID = new MethodId(METHOD_NAME, METHOD_DESCRIPTOR)
    
    private ClassInfo classInfo = new ClassInfo(CLASS_NAME)
    
    @Test
    void test_InitialValues() {
        assert classInfo.getName() == CLASS_NAME
        assert classInfo.getFields() == [] as Set
        assert classInfo.getMethods() == [] as Set
    }

    @Test
    void test_Fields() {
        def fieldInfo1 = classInfo.addField(FIELD_NAME)
        assert classInfo.getField(FIELD_NAME) == fieldInfo1
        
        assert classInfo.getFields().name as Set == [FIELD_NAME] as Set
        assert classInfo.getField(FIELD_NAME).name == FIELD_NAME
        assert classInfo.getField(FIELD_NAME).external == false
        
        def fieldInfo2 = classInfo.addExternalField(FIELD_NAME2)
        assert classInfo.getField(FIELD_NAME2) == fieldInfo2
        
        def fieldInfo3 = classInfo.addStaticFinalField(FIELD_NAME3)
        assert classInfo.getField(FIELD_NAME3) == fieldInfo3
        
        assert classInfo.getFields().name as Set == [FIELD_NAME, FIELD_NAME2, FIELD_NAME3] as Set
        assert classInfo.getField(FIELD_NAME2).external == true
        assert classInfo.getField(FIELD_NAME3).staticFinal == true
    }
    
    @Test
    void test_Methods() {
        def methodInfo = classInfo.addMethod(METHOD_NAME, METHOD_DESCRIPTOR)
        assert methodInfo.name == METHOD_NAME
        assert methodInfo.descriptor == METHOD_DESCRIPTOR
        
        assert classInfo.getMethods().name as Set == [METHOD_NAME] as Set
        assert classInfo.getMethods().descriptor as Set == [METHOD_DESCRIPTOR] as Set
        
        classInfo.getMethod(METHOD_NAME, "noSuchDescriptor") == null
        classInfo.getMethod("noSuchMethodName", METHOD_DESCRIPTOR) == null
        
        assert classInfo.getMethod(METHOD_NAME, METHOD_DESCRIPTOR) == methodInfo
        assert methodInfo.name == METHOD_NAME
        assert methodInfo.descriptor == METHOD_DESCRIPTOR

        assert classInfo.getMethod(METHOD_ID) == methodInfo
        assert classInfo.getMethod(new MethodId("noSuchMethodName", METHOD_DESCRIPTOR)) == null
        
        assert classInfo.containsMethod(METHOD_ID) == true
        assert classInfo.containsMethod(new MethodId("noSuchMethodName", METHOD_DESCRIPTOR)) == false
        
        classInfo.addMethod(METHOD_NAME2, METHOD_DESCRIPTOR2)
        assert classInfo.getMethods().name as Set == [METHOD_NAME, METHOD_NAME2] as Set
        assert classInfo.getMethods().descriptor as Set == [METHOD_DESCRIPTOR, METHOD_DESCRIPTOR2] as Set
    }

}
