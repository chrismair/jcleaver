package org.dx42.jcleaver

import org.junit.Test

/**
 * Tests for MethodId
 *
 * @author Chris Mair
 */
class MethodIdTest extends AbstractTestCase {

    private static final String FIELD_NAME = 'field123'
    
    private MethodId methodId1 =  new MethodId("m1", "d1")
    private MethodId methodId2 =  new MethodId("m2", "d2")
    
    @Test
    void test_Constructor() {
        assert methodId1.getName() == "m1"
        assert methodId1.getDescriptor() == "d1"
    }

    @Test
    void test_Equals() {
        assert methodId1 == methodId1
        assert methodId1 == new MethodId("m1", "d1")
        
        assert methodId1 != methodId2
        assert methodId1 != null
    }
    
    @Test
    void test_hashCode() {
        assert methodId1.hashCode() == methodId1.hashCode()
        assert methodId1.hashCode() == new MethodId("m1", "d1").hashCode()
        
        assert methodId1.hashCode() != methodId2.hashCode()
    }
    
    @Test
    void test_toString() {
        assert methodId1.toString() == "m1:d1"
    }
    
}
