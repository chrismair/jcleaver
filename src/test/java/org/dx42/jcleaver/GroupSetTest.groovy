package org.dx42.jcleaver

import org.junit.Test

/**
 * Tests for GroupSet
 *
 * @author Chris Mair
 */
class GroupSetTest extends AbstractTestCase {

    private static final String CLASS_NAME = 'MyClass'
    private static final String NAME = 'name123'
    private static final Group GROUP = new Group()
    private static final FieldInfo FIELD_INFO = new FieldInfo("field1")
    private static final Set<FieldInfo> IGNORED_FIELDS = [FIELD_INFO] as Set
    
    @Test
    void test_EmptyGroup() {
        def groupSet = new GroupSet(CLASS_NAME, NAME, [], [] as Set)
        assert groupSet.getClassName() == CLASS_NAME
        assert groupSet.getName() == NAME
        assert groupSet.getGroups() == []
        assert groupSet.getIgnoredFields() == [] as Set
    }
    
    @Test
    void test_Group() {
        def groupSet = new GroupSet(CLASS_NAME, NAME, [GROUP], IGNORED_FIELDS)
        assert groupSet.getClassName() == CLASS_NAME
        assert groupSet.getName() == NAME
        assert groupSet.getGroups() == [GROUP]
        assert groupSet.getIgnoredFields() == IGNORED_FIELDS
    }
    
    @Test
    void test_toString() {
        def groupSet = new GroupSet(CLASS_NAME, NAME, [], IGNORED_FIELDS)
        log(groupSet.toString())
        assert groupSet.toString() == "GroupSet [className=MyClass, name=name123, groups=[], ignoredFields=[field1]]"
    }
    
}
