package org.dx42.jcleaver

import org.junit.Test

/**
 * Tests for MethodInfo
 *
 * @author Chris Mair
 */
class MethodInfoTest extends AbstractTestCase {

    private static final String METHOD_NAME = 'method123'
    private static final String METHOD_DESCRIPTOR = 'desc123'
    private static final MethodId METHOD_ID = new MethodId(METHOD_NAME, METHOD_DESCRIPTOR)
    private static final FieldInfo FIELD_INFO1 = new FieldInfo('f1')
    private static final FieldInfo FIELD_INFO2 = new FieldInfo('f2')
    
    private MethodInfo methodInfo = new MethodInfo(METHOD_NAME, METHOD_DESCRIPTOR)
    
    @Test
    void test_Constructor() {
        assert methodInfo.getName() == METHOD_NAME
        assert methodInfo.getDescriptor() == METHOD_DESCRIPTOR
        assert methodInfo.getReadFields() == [] as Set
        assert methodInfo.getWriteFields() == [] as Set
    }

    @Test
    void test_getMethodId() {
        assert methodInfo.getMethodId() == new MethodId(METHOD_NAME, METHOD_DESCRIPTOR)
    }
    
    @Test
    void test_addReadField() {
        methodInfo.addReadField(FIELD_INFO1)
        assert methodInfo.getReadFields() == [FIELD_INFO1] as Set
    }
    
    @Test
    void test_addWriteField() {
        methodInfo.addWriteField(FIELD_INFO1)
        assert methodInfo.getWriteFields() == [FIELD_INFO1] as Set
    }
    
    @Test
    void test_addCalledByField() {
        methodInfo.addCalledByField(FIELD_INFO1)
        assert methodInfo.getCalledByFields() == [FIELD_INFO1] as Set
    }
    
    @Test
    void test_addCallsMethod() {
        methodInfo.addCallsMethod(METHOD_ID)
        assert methodInfo.getCallsMethods() == [METHOD_ID] as Set
    }
    
    @Test
    void test_addCalledByMethod() {
        methodInfo.addCalledByMethod(METHOD_ID)
        assert methodInfo.getCalledByMethods() == [METHOD_ID] as Set
    }
    
    @Test
    void test_toString() {
        methodInfo.addReadField(FIELD_INFO1)
        methodInfo.addWriteField(FIELD_INFO2)
        methodInfo.addCallsMethod(METHOD_ID)
        methodInfo.addCalledByMethod(METHOD_ID)
        log(methodInfo.toString())
        assert methodInfo.toString() == "MethodInfo [methodId=method123:desc123, readFields=[f1], writeFields=[f2], calledByFields=[], callsMethods=[method123:desc123], calledByMethods=[method123:desc123]]"
    }
    
}
