package org.dx42.jcleaver

import org.dx42.jcleaver.report.GroupSetReportWriter
import org.dx42.jcleaver.report.TextGroupSetReportWriter
import org.dx42.jcleaver.sample.Sample_Lambdas
import org.dx42.jcleaver.sample.Sample_MethodsUsedOnlyByFields
import org.dx42.jcleaver.sample.Sample_OneGroup
import org.dx42.jcleaver.sample.Sample_OneGroup_OnlyFieldsInCommon
import org.dx42.jcleaver.sample.Sample_StaticFields
import org.dx42.jcleaver.sample.Sample_ThreeGroups
import org.dx42.jcleaver.sample.Sample_TwoGroups
import org.dx42.jcleaver.sample.Sample_UsesSuperclassFields
import org.dx42.jcleaver.sample.Sample_UsesSuperclassMethods
import org.junit.Test

/**
 * Tests for DefaultAnalyzer
 *
 * @author Chris Mair
 */
class DefaultAnalyzerTest extends AbstractTestCase {

    private Analyzer analyzer = new DefaultAnalyzer()
    private ClassParser classParser = new AsmClassParser()
    
    @Test
    void test_ImplementsAnalyzer() {
        assert analyzer instanceof Analyzer
    }

    @Test
    void test_analyze_OneGroup() {
        def classInfo = classParser.parseClass(Sample_OneGroup.getName())
        GroupSet groupSet = analyzer.analyze(classInfo)
        
        assertGroupSet(groupSet, Sample_OneGroup, 1)
        assertGroup(groupSet, ['field1', 'field2', 'field3', 'field4'], ['method1', 'method2', 'method3', 'method4'])
    }

    @Test
    void test_analyze_OneGroup_OnlyFieldsInCommon() {
        def classInfo = classParser.parseClass(Sample_OneGroup_OnlyFieldsInCommon.getName())
        GroupSet groupSet = analyzer.analyze(classInfo)
        
        assertGroupSet(groupSet, Sample_OneGroup_OnlyFieldsInCommon, 1)
        assertGroup(groupSet, ['field1', 'field2', 'field3', 'field4'], ['method1', 'method2', 'method3', 'method4'])
    }
    
    @Test
    void test_analyze_TwoGroups() {
        def classInfo = classParser.parseClass(Sample_TwoGroups.getName())
        GroupSet groupSet = analyzer.analyze(classInfo)
        
        assertGroupSet(groupSet, Sample_TwoGroups, 2)
        assertGroup(groupSet, ['field1', 'field2'], ['method1', 'method2'])
        assertGroup(groupSet, ['field3', 'field4'], ['method3', 'method4'])
    }

    @Test
    void test_analyze_ThreeGroups() {
        def classInfo = classParser.parseClass(Sample_ThreeGroups.getName())
        GroupSet groupSet = analyzer.analyze(classInfo)
        
        assertGroupSet(groupSet, Sample_ThreeGroups, 3)
        assertGroup(groupSet, ['field1', 'field2'], ['method1', 'method2'])
        assertGroup(groupSet, ['field3', 'field4'], ['method3', 'method4'])
        assertGroup(groupSet, ['field5'], ['method5'])
    }
    
    @Test
    void test_analyze_StaticFields() {
        def classInfo = classParser.parseClass(Sample_StaticFields.getName())
        GroupSet groupSet = analyzer.analyze(classInfo)
        
        assertGroupSet(groupSet, Sample_StaticFields, 2)
        assertIgnoredFields(groupSet, ['CONSTANT1'])
        assertGroup(groupSet, ['CONSTANT2'], ['method1', 'method2'])
        assertGroup(groupSet, ['counter'], ['method3', 'method4'])
    }

    @Test
    void test_analyze_UsesSuperclassFields() {
        def classInfo = classParser.parseClass(Sample_UsesSuperclassFields.getName())
        log(classInfo)
        GroupSet groupSet = analyzer.analyze(classInfo)
        
        assertGroupSet(groupSet, Sample_UsesSuperclassFields, 1)
        assertGroup(groupSet, ['field1', 'field2', 'superField1', 'superField2'], ['method1', 'method2', 'method3'])
    }

    @Test
    void test_analyze_UsesSuperclassMethods() {
        def classInfo = classParser.parseClass(Sample_UsesSuperclassMethods.getName())
        log(classInfo)
        GroupSet groupSet = analyzer.analyze(classInfo)
        
        assertGroupSet(groupSet, Sample_UsesSuperclassMethods, 2)
        assertGroup(groupSet, ['field1'], ['method2', 'superMethod2'])
        assertGroup(groupSet, [], ['method1', 'method3', 'superMethod1'])
    }

    @Test
    void test_analyze_Lambdas() {
        def classInfo = classParser.parseClass(Sample_Lambdas.getName())
        GroupSet groupSet = analyzer.analyze(classInfo)
        
        assertGroupSet(groupSet, Sample_Lambdas, 2)
        assertGroup(groupSet, ['field1', 'field2'], ['method1', 'method2'])
        assertGroup(groupSet, ['field3', 'field4', 'field5', 'field6'], ['method3', 'method4'])
    }

    @Test
    void test_analyze_MethodsUsedOnlyInFieldInitializers() {
        def classInfo = classParser.parseClass(Sample_MethodsUsedOnlyByFields.getName())
        GroupSet groupSet = analyzer.analyze(classInfo)
        printGroupSet(groupSet)
        
//        assertGroupSet(groupSet, Sample_TwoGroups, 2)
//        assertGroup(groupSet, ['field1', 'field2'], ['method1', 'method2'])
//        assertGroup(groupSet, ['field3', 'field4'], ['method3', 'method4'])
    }

    // TODO Test for class with no methods
    // TODO Test for class with unused fields
    // TODO Should all methods referencing ANY superclass fields be put in the same group?
    
    //------------------------------------------------------------------------------------
    // Helper methods 
    //------------------------------------------------------------------------------------
    
    private void assertGroupSet(GroupSet groupSet, Class theClass, int expectedSize) {
        assert groupSet.getClassName().replace('/', '.') == theClass.getName()
        assert groupSet.getName() == DefaultAnalyzer.DEFAULT_GROUP_SET_NAME
        assert groupSet.getGroups().size() == expectedSize
    }
    
    private void assertIgnoredFields(GroupSet groupSet, Collection ignoredFieldNames) {
        assert groupSet.getIgnoredFields().name as Set == ignoredFieldNames as Set
    }
    
    private void assertGroup(GroupSet groupSet, Collection fieldNames, Collection methodNames) {
        List<Group> groups = groupSet.getGroups()
        boolean match = groups.find { group -> 
                (group.getFields().name as Set == fieldNames as Set) &&
                (group.getMethods().name as Set == methodNames as Set)
        }
        assert match, "Group with fieldNames=$fieldNames; methodNames=$methodNames"
    }
    
    private void printGroupSet(GroupSet groupSet) {
        GroupSetReportWriter reportWriter = new TextGroupSetReportWriter()
        reportWriter.writeReport(groupSet)
    }
    
}
